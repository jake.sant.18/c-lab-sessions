//
// Created by Jake Sant on 16/10/2019.
//

#include <stdio.h>

void butler(void);

char name[4] = "Jake";

int main(void)
{
    butler();

    printf("I will summon the butler function.\n");
    butler();
    printf("Yes. Bring me some tea and writeable CD-ROMs.\n");

    return 0;
}

void butler(void)
{
    printf("You rang, sir %s?\n", name);
}