//
// Created by Jake Sant on 23/10/2019.
//

#include <stdio.h>

int main(void)
{
    int a3 = 1000;
    int a4 = 1000;
    int a5 = 1000;

    int i = 0; //Used for loops

    int oa3, oa4, oa5; //Orders

    printf("How many packs of A3 paper would you like to order?\n");
    while(i == 0)
    {
        scanf("%d", &oa3);
        if(oa3 > 1000 || oa3 <0)
            printf("That value is invalid! Please enter another order number.\n");
        else
            i++;
    }

    i == 0;

    printf("How many packs of A4 paper would you like to order?\n");
    while(i == 0)
    {
        scanf("%d", &oa4);
        if(oa4 > 1000 || oa4 <0)
            printf("That value is invalid! Please enter another order number.\n");
        else
            i++;
    }

    i == 0;

    printf("How many packs of A5 paper would you like to order?\n");
    while(i == 0)
    {
        scanf("%d", &oa5);
        if(oa5 > 1000 || oa5 <0)
            printf("That value is invalid! Please enter another order number.\n");
        else
            i++;
    }

    i == 0;

    a3 = a3 - oa3;
    a4 = a4 - oa4;
    a5 = a5 - oa5;

    printf("The remaining stock is as follows\n A3 -- %d\n A4 -- %d\n A5 -- %d", a3, a4, a5);
    return 0;
}