//
// Created by Jake Sant on 07/11/2019.
//

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void displayMenu(void);
void addItems();
void showTotal();
void checkout();
void cancelSession();
void question10(void);

float total_cost = 0;
float price[10];
char item_name[10][20] = { "Potatoes", "Shampoo", "Bread", "Batteries", "Soup", "Salt",
                           "Water", "Chocolate", "Coffee", "Tea"  };

//First index in char refers to words, second index refers to letters

char item_code[8][10]; //8 digit item code

void question10(void)
{

    //Use enums for QUIT, ADD_ITEM, etc... instead of a switch case
    /*typedef enum InputType
    {
        ADD_ITEM
        QUIT
        ETC...
    } InputType;
    */
    printf("Welcome to the shopping system\n");
    printf("Please enter a number to select an option or press q to exit the system\n\n");
    printf("1. Add items to shopping cart\n2. Show current total\n");
    printf("3. Check out\n4. Cancel session\nq. Quit\n");

    char choice = getchar();

    switch (choice)
    {
        case '1':
            addItems();
            break;

        case '2':
            showTotal();
            break;

        case '3':
            //checkout();
            break;

        case '4':
            //cancelSession();
            break;

        case 'q':
            printf("Exiting...");
            break;
    }
}

void addItems(void)
{
    printf("Code \t-\t Name \t-\t Price\n");
    for(int i = 0; i < 30; i++)
    {
            if (item_name[i] != '\0')
            {
                printf("%s", item_name[i]);
            }
        }
        printf("\n");
    }
}

void showTotal(void)
{
    printf("Your total bill is currently %.2f", total_cost);
}

int main(void)
{
    question10();
    return 0;
}