//
// Created by Jake Sant on 30/10/2019.
//

#include <stdio.h>
#include <math.h>

//Reimann zeta function is a summation function that starts from n = 1 and continues to infinity.
//The actual function is n^(-s) or 1/(n^s)

int main(void)
{
    int n;
    double re; //Will hold value of Reimann zeta function
    double s; //The complex variable?

    printf("The Riemann zeta function is represented by C(S), where S is a complex variable.\n");
    printf("Enter a value for N:\n");
    scanf("%d", &n);
    printf("Enter a value for S:\n");
    scanf("%lf", &s);

    for(int i = n; i != 0; i--)
    {
        re = re + pow(i, (-s)); //pow is a function from math.h which will compute 1/(n^s)
    }

    printf("The Riemann zeta function for %lf is computed as %lf", s, re);
    return 0;
}
