// This contains a list of examples relating to pointers

#include <stdio.h>

void AlterValue(int *temp_val)
{
    *temp_val = 40; //Dereferencing a pointer's address gives the value stored in memory there
}

int main()
{
    int temp_1 = 60;
    int *temp_1_pointer = &temp_1; //Asterisk * is used to create an integer as a pointer. & points to a normal integer's address in memory
    printf("%d %d\n", temp_1_pointer, *temp_1_pointer);
    AlterValue(temp_1_pointer);
    printf("%d %d\n", temp_1_pointer, *temp_1_pointer);

    AlterValue(&temp_1);

    char my_array[] = {'a', 'b', 'c'};

    char *my_array_pointer = my_array; //No need for & because array without the [] already refers to an address

    for(int index = 0; index < 3; index++)
    {
        printf("%c \n", *(my_array_pointer + index)); //+ index is pointing to the next element in the char array
        //each element in char takes up 1 byte
    }

    return 0;
}