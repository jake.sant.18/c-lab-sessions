//
// Created by Jake Sant on 28/10/2019.
//

#include <stdio.h>

void factorial(int n); //Using for loop
void factrecur(int n); //Recursively

int main(void)
{
    int n;

    printf("This program will compute a factorial (n!).\n");
    printf("Enter the value of n.\n");
    scanf("%d", &n);

    factorial(n);


    return 0;
}

void factorial(int n)
{
    int n1 = n;
    for(int i = n-1; i !=0; i--)
    {
        n = n * i;
    }

    printf("%d! = %d", n1, n);
}

int factrecur(int n)
{
    return n*factrecur(n-1);
}
