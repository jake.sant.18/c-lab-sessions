//
// Created by Jake Sant on 13/11/2019.
//
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h> //Used for isdigit() function
#include <stdbool.h>

void question2(void)
{
    //Implementation of an iterative and recursive version of the Fibonacci Sequence
    //The sequence is of the pattern 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...
    //For all i > 2, a term of the pattern is found by adding the 2 terms before it
    //fibonacci(i-1) + fibonacci(i-2)

    int n; //Number of terms
    int t1 = 0, t2 = 1, nextTerm;
    printf("Enter the number of terms.\n");
    scanf("%d", &n);

    for (int i = 1; i <= n; ++i)
    {
        printf("%d, ", t1);
        nextTerm = t1 + t2;
        t1 = t2;
        t2 = nextTerm;
    }


}

int gcd(int x, int y);

void question3(void) //Program to calculate greatest common divisor
{
    /*greatest common divisor (gcd) of two or more integers, which are not all zero,
     * is the largest positive integer that divides each of the integers.
     * For example, the gcd of 8 and 12 is 4.*/
    int x, y;

    printf("Enter the first positive integer.\n");
    scanf("%d", &x);
    printf("Enter the second positive integer.\n");
    scanf("%d", &y);


    int current_x = x; //Saving current state of x
    int current_y = y; //Saving current state of y

    printf("First, the GCD will be calculated recursively\n");
    printf("The greatest common divisor of %d and %d is %d\n", x, y, gcd(x,y));

    printf("And now the GCD will be calculated iteratively\n");

    while (current_y != 0)
    {
        int temp_x = current_y;
        current_y = current_x % current_y; //gcd(y, remainder(x,y))
        current_x = temp_x;
    }

    printf("The greatest common divisor of %d and %d is %d\n", x, y, current_x);
}

int gcd(int x, int y)
{
    if (y == 0)
    {
        return x;
    }
    else
    {
        return gcd(y, x%y);
    }
}

void question4(void) //Do at home
{
    int num1, decimal, rem;
    printf("Enter a number.\n");
    scanf("%d", &num1);

    decimal = num1/16;
    rem = num1%16;

    printf("Num 2 is %d, remainder is %d", decimal, rem);
}

void stringFunc(char *string);
void question5(void)
{
    char seq[50];
    printf("Kindly enter a string sequence.\n");
    scanf("%s", &seq);

    stringFunc(seq);
}

void stringFunc(char *string) //pass results of validity, length, most frequent letter and first letter as * parameters
{
    int max;
    char common;
    int freq[256] = {0}; //Can change this to type short since this won't be a large
    int len = strlen(string);

    for(int i = 0; string[i] != '\0'; i++) //Loops through sequence and checks if there is a number
    {
        if(isdigit(string[i]))
        {
            printf("ERROR: String sequence contains a numeric character.\nPlease try again.\n");
            goto endOfScope; //clear values in seq[] and call question5() again instead of using goto?
        }
    }

    if(isspace(string[0])) //Checks if the string being with a space
    {
        printf("ERROR: String sequence begins with a space.\n");
        goto endOfScope;
    }

    printf("The string sequence is %d characters long.\n", len);

    printf("The first character of the sequence is %c.\n", string[0]);

    //*first_char = *string; -- An array has a pointer to the first element. *string dereferences the pointer
    //and gives the first element in the array instead. This can be done instead of string[0]

    for(int i = 0; string[i] != '\0'; i++)
    {
        freq[string[i]]++; //Can possibly move this into the first for loop to check for numeric character
    }

    for(int i = 0; string[i] != '\0'; i++) //Move into previous loop, might be more efficient
    {
        if(max < freq[string[i]])
        {
            max = freq[string[i]];
            common = string[i];
        }
    }

    printf("The most frequent character in the sequence is %c.\n", common);
    endOfScope:
    printf("\n");
}


void question6(void)
{
    //ceil(x) function returns the smallest integer that is greater or equal to x. This rounds up
    //floor(x) function returns the largest integer that is smaller or equal to x. This rounds down
    double x, value1, value2;

    printf("Please enter any real number.\n");
    scanf("%lf", &x);

    value1 = floor(x); //Pass this by reference
    value2 = ceil(x); //Pass this by reference

    printf("The floor value of x is %.1f\n", value1);
    printf("The ceiling value of x is %.1f\n", value2);
}

void question7(void) //Using the ceil(x) and floor(x) functions without actually using them
{
    double x;
    int value1;

    printf("Please enter any real number.\n");
    scanf("%lf", &x);

    value1 = (int) (x*10);

    printf("Value 1 after truncating is %d\n", value1);
    int decim = value1 % 10;

    printf("The decimal is %d\n", decim);
    value1 = (value1-decim)/10;

    if(decim >= 5)
    {
        value1++;
        printf("The ceiling function gives an answer of %d\n",value1);
    }
    else
        if (decim < 5)
    {
        value1--;
        printf("The floor function gives an answer of %d\n", value1);
    }
}

int main(void)
{
    int input;

    bool exit = false; //False is a 0 value
    while (!exit) //Question 8, displaying menu
    {
        //scanf("%s[^\n]s", input);

        //Can implement menu as 2D array or char **menu
        printf("Select which function you wish to perform from the menu\nbelow by entering the corresponding number.\n");
        printf("1. Fibonacci Sequence \t 2. Greatest Common Divisor\n");
        printf("3. Decimal to Hex \t 4. String Parser\n");
        printf("5. Ceiling and Floor \t 6. Exit\n");

        scanf("%d", &input);

        if(input < 1 || input > 6)
        {
            printf("The number you entered is invalid. Please enter another number.\n");
        }
        exit = (input == 6);
    }
    return 0;
}