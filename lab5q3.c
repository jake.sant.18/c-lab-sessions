// Created by Jake Sant on 03/12/2019.

// sum_arr1.c -- sums the elements of an array
// use %u or %lu if %zd doesn't work

#include <stdio.h>
#define SIZE 10

int sum(int ar[], int *n_ptr);
int main(void)
{
    int marbles[SIZE] = {20,10,5,39,4,16,19,26,31,20};
    long answer;

    int *p = &marbles;

    answer = sum(p, &SIZE);

    printf("The total number of marbles is %ld.\n", answer);
    printf("The size of marbles is %u bytes.\n", sizeof *p);
    return 0;
}

int sum(int * p, int *n)     // how big an array?
{
    int i;
    int total = 0;

    for( i = 0; i < n; i++)
        total += p[i];

    printf("The size of ar is %u bytes.\n", sizeof &p);

    return total;
}
