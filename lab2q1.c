//
// Created by Jake Sant on 23/10/2019.
//

#include <stdio.h>

int main(void)
{
    float weight;    /* user weight            */

    double value;     /* platinum equivalent     */



    printf("Are you worth your weight in platinum?\n");

    printf("Let's check it out.\n");

    printf("Please enter your weight in pounds:\n");



/* get input from the user                     */

    scanf("%f", &weight);

/* assume platinum is $1770 per ounce            */

/* 14.5833 converts pounds avd. to ounces troy */

    value = 1770.0 * weight * 14.5833;

    printf("Your weight in platinum is worth $%.4f.\n", value);

    printf("You are easily worth that! If platinum prices drop,\n");

    printf("eat more to maintain your value.\n");



    /* If value was changed to an int, with the %d parameter, the value rounds to the nearest decimal.
     * Using the %f parameter gives a logical error, with the result being 0.00000.
     */

    return 0;

}

