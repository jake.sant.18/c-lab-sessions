//
// Created by Jake Sant on 16/10/2019.
//

#include <stdio.h>

void add(void);
void subtract(void);
void multiply(void);

int n1, n2;

int main(void)
{


    printf("Please enter the value for the first number\n");
    scanf("%d", &n1);
    printf("Please enter the value for the second number\n");
    scanf("%d", &n2);

    add();
    subtract();
    multiply();
}

void add(void)
{
    int ans1 = n1 + n2;
    printf("%d and %d added together results in %d \n", n1, n2, ans1);

}

void subtract(void)
{
    int ans2 = n1 - n2;
    printf("Subtracting %d from %d gives a result of %d \n", n2, n1, ans2);

}

void multiply(void)
{
    int ans3 = n1 * n2;
    printf("Multiplying %d and %d gives a result of %d \n", n1, n2, ans3);
}