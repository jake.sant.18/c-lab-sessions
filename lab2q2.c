//
// Created by Jake Sant on 24/10/2019.
//

#include <stdio.h>

int main(void)
{
    int num;
    char cs;

    printf("Enter a number:\n");
    scanf("%d", &num);


    cs = num;
    printf("The character set in ASCII for that number is %c", cs);
    printf("The number in octal format is %o", cs);
    printf("The number in hexadecimal format is %#x", cs);


    return 0;
}