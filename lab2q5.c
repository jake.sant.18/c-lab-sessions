//
// Created by Jake Sant on 28/10/2019.
//

#include <stdio.h>
#define DAYSINWEEK 7

int main(void)
{
    int weeks, days, idays;

    for (int i = 0; i < 10; i++) {

        printf("Enter a number of days:\n");
        scanf("%d", &idays);

        weeks = idays / DAYSINWEEK;
        days = idays % DAYSINWEEK;

        printf("%d is the same as %d weeks and %d days", idays, weeks, days);
    }
    return 0;
}