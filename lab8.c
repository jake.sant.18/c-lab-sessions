// Created by Jake Sant on 18/12/2019.

//Shared libraries

/* A library is a set of functionality, it contains the actual implementation and file source of functions, which can be called in programs
 *
 * To create a library in CMakeLists.txt, you need to do:
 * add_library(Name source.c)
 * add_targetlibrary(Name) */

//Use for task 2 in assignment

#include <stdlib.h>
#include <stdio.h>
#include "int_array.h"

int main(void)
{
    int my_int = 400;
    int * my_int_ptr = &my_int;

    int * another_ptr = malloc(sizeof(int));
    * another_ptr = 500; //Dereferenced and assigned a value


    array_int_t * my_array = initIntArray(100);
    populateArray(my_array);
    printValues(my_array);
    deleteArray(my_array);
    getchar(); //So the console doesn't stop immediately
    return 0;
}