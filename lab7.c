//
// Created by Jake Sant on 18/12/2019.
//

//dyn_arr.c -- dynamically allocated array for question 1
//booksave.c -- saves structure contents in a file for question 2

#include <stdio.h>
#include <stdlib.h> //for malloc(), free()
#include "sgets.h"
#define MAXTITL 40
#define MAXAUTL 40
#define MAXBKS 8 //Maximum number of books

void question1(void);
void question2(void);

struct book
{                   /* set up book template    */
    char title[MAXTITL];
    char author[MAXAUTL];
    float value;
};

void question1(void)
{
    double * ptd;
    int max;
    int number;
    int i = 0;

    puts("What is the maximum number of type double entries?");
    scanf("%d", &max);
    ptd = (double *) malloc(max * sizeof (double));

    //If we remove the call to malloc in the line above, the program will crash after inserting any values

    if (ptd == NULL) //This will only be called if we remove malloc in the line above, and then insert 0 into max
    {
        puts("Memory allocation failed. Goodbye.");
        exit(EXIT_FAILURE);
    }

    /* ptd now points to an array of max elements */

    puts("Enter the values (q to quit):");

    while (i < max && scanf("%lf", &ptd[i]) == 1)
        ++i;

    printf("Here are your %d entries:\n", number = i);

    for (i = 0; i < number; i++)
    {
        printf("%7.2f ", ptd[i]);

        if (i % 7 == 6)
            putchar('\n');
    }

    if (i % 7 != 0)
        putchar('\n');

    puts("Done.");
    free(ptd);
}

void question2(void)
{
    struct book* library = malloc(sizeof(struct book) * MAXBKS); //Allocated to heap
    int capacity = MAXBKS;
    //An array of the structure book, this is placed on the stack

    int count = 0;
    int index, filecount;
    FILE *pbooks; //A file pointer to the data type FILE is declared, and we call the pointe rpbooks
    int size = sizeof(struct book);

    if ((pbooks = fopen("book.dat", "a+b")) == NULL) {
        //pbooks = fopen -- opens the the file called book.dat
        //Mode a opens the file, loads it into memory and sets a pointer to the last character in it
        //if there is no file it will create it. Mode b opens as a binary file

        fputs("Can't open book.dat file\n", stderr); //Error will be returned if file can't be opened for some reason
        exit(1);
    }

    rewind(pbooks);  //Moves pointer to point to beginning of file

    while (count < MAXBKS && fread(&library[count], size, 1, pbooks) == 1) {
        if (count == 0)
            puts("Current contents of book.dat:"); //Will initially show what's already in file

        printf("%s by %s: $%.2f\n", library[count].title, library[count].author, library[count].value);
        //library[count] -> title is the same as (*library).count
        count++;
        //Outputs current contents of file
    }

    filecount = count;

    if (count == capacity) //Fix variable capacity and program works
    {
        printf("The book.dat file is full. Allocating more memory now.\n");
        //fputs("The book.dat file is full. Allocating more memory now.\n", pbooks);
        realloc(library, sizeof(library) * 2);
        capacity = sizeof(library) * 2;
    }

    puts("Please add new book titles.");
    puts("Press [enter] at the start of a line to stop.");

    while (count < MAXBKS && sgets(library[count].title, MAXTITL) != NULL && library[count].title[0] != '\0')
    {
        puts("Now enter the author.");
        sgets(library[count].author, MAXAUTL);
        puts("Now enter the value.");
        scanf("%f", &library[count++].value);

        while (getchar() != '\n') //The loop will continue asking for the next title so long as an enter isn't entered
            continue;             //Will continue to the next lines

        if (count < MAXBKS)
            puts("Enter the next title.");
    }

    if (count > 0)
    {
        puts("Here is the list of your books:");

        for (index = 0; index < count; index++)
            printf("%s by %s: $%.2f\n", library[index].title, library[index].author, library[index].value);

        //First everything previously saved along with what you entered will be printed out

        fwrite(&library[filecount], size, count - filecount, pbooks);
        //Now the ADDITIONAL books will be saved to the file
    }
    else
        puts("No books? Too bad.\n");

    puts("Bye.\n");
    fclose(pbooks); //Closes the file that is being pointed to by the pointer pbooks
}

int main(void)
{
    question2();
    return 0;
}