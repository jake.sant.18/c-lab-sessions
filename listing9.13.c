/* swap1.c -- first attempt at a swapping function */

#include <stdio.h>

void interchange(int *u, int *v); /* declare function */

int main(void)
{
    int x = 5, y = 10;

    printf("Originally x = %d and y = %d.\n", x , y);

    printf("x: lvalue/address %p rvalue %d\n", &x, x);
    printf("y: lvalue/address %p rvalue %d\n", &y, y);

    interchange(&x, &y);

    printf("Now x = %d and y = %d.\n", x, y);



    return 0;
}



void interchange(int *u, int *v)  //* syntax points to the address of u and v in memory

{

    int temp;

    //printf("u: stores x's address %p whose value is %d\n", u, *u):
    //printf("v: stores y's address %p whose value is %d\n", v, *v):

    temp = *u;

    *u = *v;

    v = temp;

}

list9_13.txt
        Displaying list9_13.txt.