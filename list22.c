#include <stdio.h>

int main(void)
{
    int feet0, fathoms0;

    double feet1, fathoms1;

    float feet2, fathoms2;

    fathoms0 = 2;
    fathoms1 = 2;
    fathoms2 = 2;

    feet0 = 6 * fathoms0;
    feet1 = 6 * fathoms1;
    feet2 = 6 * fathoms2;

    printf("Integer format: There are %d feet in %d fathoms!\n", feet0, fathoms0);
    printf("Double format: There are %f feet in %f fathoms!\n", feet1, fathoms1);
    printf("Float format: There are %f feet in %f fathoms!\n", feet2, fathoms2);



    return 0;
    }