//
// Created by Jake Sant on 13/11/2019.
//

/* arf.c -- array functions */

#include <stdio.h>
#define SIZE 5

void show_array(const double *ar, int n); //OR const double *ar. This is pointer notation for the array. We can still use standard array notation though
void mult_array(double ar[], int n, double mult);

int main(void)
{
    double dip[SIZE] = {20.0, 17.66, 8.2, 15.3, 22.22};

    printf("The original dip array:\n");
    show_array(dip, SIZE);

    mult_array(dip, SIZE, 2.5);

    printf("The dip array after calling mult_array():\n");

    show_array(dip, SIZE);

    return 0;
}



/* displays array contents */

void show_array(const double *ar, int n) //OR ar[]
{
    int i;

    for (i = 0; i < n; i++)
        printf("%8.3f ", *(ar++)); //++ Does not increment by 1 but by 8 bytes - the size between the next index in the array

    putchar('\n');
}

/* multiplies each array member by the same multiplier */

void mult_array(double ar[], int n, double mult)
{
    int i;

    for (i = 0; i < n; i++)
        ar[i] *= mult;
}