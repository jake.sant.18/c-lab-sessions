//Shared libraries

/* A library is a set of functionality, it contains the actual implementation and file source of functions, which can be called in programs
 *
 * To create a library in CMakeLists.txt, you need to do:
 * add_library(Name source.c)
 * add_targetlibrary(Name) */

//Use for task 2 in assignment

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "int_array.h"

array_int_t * initIntArray(int size)
{
    array_int_t *temp_array = malloc(sizeof(array_int_t));
    temp_array->array = malloc(sizeof(int) * size);
    temp_array->length = size;
    return temp_array;
}

void populateArray(array_int_t *temp_array)
{
    srand(time(NULL));
    for(int index = 0; index < temp_array->length; index++)
    {
        temp_array->array[index] = rand % 100;
    }
}

void deleteArray(array_int_t *temp_array)
{
    free(temp_array->array);
    temp_array->array = NULL;
    free(temp_array);
}

void printValues(array_int_t *temp_array)
{
    for(int index = 0; index < temp_array->length; index++)
    {
        printf("%d \t", temp_array->array[index]);
    }
    printf("\n");
}

int main(void)
{
    int my_int = 400;
    int * my_int_ptr = &my_int;

    int * another_ptr = malloc(sizeof(int));
    * another_ptr = 500; //Dereferenced and assigned a value


    array_int_t * my_array = initIntArray(100);
    populateArray(my_array);
    printValues(my_array);
    deleteArray(my_array);
    getchar(); //So the console doesn't stop immediately
    return 0;
}