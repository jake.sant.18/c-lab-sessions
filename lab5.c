#include <stdio.h>
#define SIZE 10


void question1(void);
void question4(void);
void question5(void);

void question1(void)
{
    int arr[10] = {2,4,6,8,10,12,14,16,18,20};
    int copy_arr[10];

    for(int i = 0; i < 10; i++)
    {
        copy_arr[i] = arr[i];
    }

    for(int i = 0; i < 10; i++)
    {
        printf("Number %d is %d\n", i, copy_arr[i]);
    }
}

int sump(int * start, int * end)
{
    //Use pointer arithmetic
    int total = 0;

    while (start < end)
    {
        total += *start; /* add value to total              */
        start++;         /* advance pointer to next element */
    }

    return total;
}

void question4(void)
{
    /* sum_arr2.c -- sums the elements of an array */

    int sump(int * start, int * end);

    int marbles[SIZE] = {20,10,5,39,4,16,19,26,31,20};
    long answer;

    answer = sump(marbles, marbles + SIZE);
    printf("The total number of marbles is %ld.\n", answer);
}

void question5(void)
{
    //Implement f(x,y,z)=x+6y+7.2z in a 3d array for all independent variable value combinations ranging from 0­100.


    //Need to increase stack size
    
    int x, y, z;
    int function[101][101][101];

    for(x = 0; x <= 100; x++)
    {
        for(y = 0; y <= 100; y++)
        {
            for(z = 0; z <= 100; z++)
            {
                function[x][y][z] = x + (6*y) + (7.2*z);
            }
        }
    }

    for(x = 0; x <= 100; x++)
    {
        for(y = 0; y <= 100; y++)
        {
            for(z = 0; z <= 100; z++)
            {
                printf("f(%d, %d, %d) = %d", x, y, z, function[x][y][z]);
            }
        }
    }


}

int main(void)
{
    question4();

    return 0;
}