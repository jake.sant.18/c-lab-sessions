#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//This is used as a generic .c file so that I can follow the slides during lessons

int main(int argc, char *argv[])

{

    int ch;         // place to store each character as read

    FILE *fp;       // "file pointer"

    unsigned long count = 0;

    const char *filename = "myfile.txt"; //This is a relative path, where the relative path is the location of the .exe


    if ((fp = fopen(filename, "r")) == NULL)

    {

        printf("Can't open %s\n", filename);

        exit(1);

    }

    while ((ch = getc(fp)) != EOF)

    {

        putc(ch,stdout);  // same as putchar(ch);

        count++;

    }

    fclose(fp);

    printf("File %s has %lu characters\n", filename, count);



    return 0;

}

