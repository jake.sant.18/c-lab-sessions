//
// Created by Jake Sant on 30/10/2019.
//

#include <stdio.h>
#define EXCHANGE 0.90

int main(void)
{
    float dol[10];
    float eur[10];

    for (int i = 0; i < 10; i++)
    {
        printf("Enter value %d in dollars\n", i+1);
        scanf("%f", &dol[i]);
        eur[i] = dol[i] * EXCHANGE;
    }

    for (int i = 0; i < 10; i++)
    {
        printf("$%.2f\t EUR%.2f\n", dol[i], eur[i]);
    }
}
