//
// Created by Jake Sant on 30/10/2019.
//


#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h> //Needed for isupper() function


#define TAXRATE2 0.2

int question1(void)
{
    long num;
    long sum = 0L;      /* initialize sum to zero   */
    int status;

    for(int i = 0; status == 1; i++) /* == means "is equal to"   */
    {
        status = scanf("%ld", &num);
        sum = sum + num;
        printf("Please enter next integer (q to quit): ");
    }

    printf("Those integers sum to %ld.\n", sum);

    return 0;
}

void question2(int n)
{
    int n1 = n;
    for(int i = n-1; i !=0; i--)
    {
        n = n * i;
    }

    printf("%d! = %d", n1, n);
}

void question3(void)
{
    int pin;
    int attempt;

    printf("Enter a PIN\n");
    scanf("%d", &pin);

    for(int tries = 5; tries >= 1; tries--)
    {
        printf("Now, try guessing the PIN. You have %d tries\n", tries);
        scanf("%d", &attempt);

        if(attempt == pin)
        {
            printf("You have guessed the PIN correctly! Nice job!");
            break;
        }   else
            printf("Wrong! Guess again.");

        if(attempt != pin && tries <1)
            printf("You have run out of tries. Better luck next time.");
    }
}

void question4(void)
{
    int i, j;

    for(i=1;i<=10;i++)
    {
        for (j = 1; j <= 10; j++)
        {
            printf("%d\t", j * i);
        }
        printf("\n");

    }
}


void question5(void)
{
    int seq[20];
    double mean, median;
    int current_num = 0;
    int current_count = 0;
    int max_num = 0;
    int max_count = 0;

    printf("Enter up to 20 UNIQUE numbers between 1 and 100.\n");
    printf("Kindly enter them in ASCENDING order\n");
    printf("If you are entering less than 20 numbers, enter q to quit\n");

    for(int i = 0; i <= 20; i++)
    {
        printf("Enter a number or enter q to quit\n");
        scanf("%d", &seq[i]);
    }

    //Mode
    for(int index = 0; index < 20; index ++)
    {
        if(current_num != seq[index])
        {
            if(max_count < current_count)
            {
                max_count = current_count;
                max_num = current_num;
            }

            current_count = 1;
            current_num = seq[index];
        } else
        {
            current_count++;
        }
    }

}


void question6(void)
{
    double salary, totaltax;
    double tax1paid = 0;
    double tax2paid = 0;
    double tax3paid = 0;
    int bracket1 = 10000;
    double taxrate1 = 0.18;
    double taxrate3 = 0.25;
    int i;
    bool ict;

    printf("Kindly enter your salary\n");
    scanf("%lf", &salary);

    printf("Do you work in the ICT industry?\n1. Yes\n2. No\n");
    scanf("%d", &i);

    while(i != 1 && i != 2)
    {
        printf("Please enter either 1 or 2.\n");
        scanf("%d", &i);
    }

    if((i == 1))
    {
        ict = true;
    }

    i = 0;

    printf("Do collect old electronic equipment for green disposal?\n1. Yes\n2. No\n");
    scanf("%d", &i);

    while(i != 1 && i != 2)
    {
        printf("Please enter either 1 or 2.\n");
        scanf("%d", &i);
    }

    if((i == 1))
    {
        bracket1 = 15000;
    }

    if(salary <= bracket1)
    {
        tax1paid = salary * taxrate1;
        salary = salary - tax1paid;
    }

    if(0 < salary && salary <= 8000)
    {
        tax2paid = salary * TAXRATE2;
        salary = salary - tax2paid;
    }


    if(0 < salary)
    {
        tax3paid = salary * taxrate3;
        salary = salary - tax3paid;
    }

    totaltax = tax1paid + tax2paid + tax3paid;

    if((ict == true))
    {
        totaltax = totaltax * 0.95;
    }


    printf("The total tax you have to pay is %.2f\n", totaltax);
}

void question7(void)
{
    char word[30];
    int cur_char_id = 0;
    int word_size = 0;
    int total_errors = 0;
    int word_count = 0;
    bool found_space = false;
    bool contains_hyphen = false;

    printf("Enter a word\n");
    scanf("%[^\n]s", word); //%s not used because it will only take up until the first space

    int l = strlen(word);

    while(cur_char_id < l)
    {
        switch(word[cur_char_id]) //Will examine the index of the word at the current position
        {
            case ' ':
                if(found_space == true)
                {
                    total_errors++; //Gives error if there was already a space
                } else
                {
                    found_space = true;
                }
                break;

            case '`':
                found_space = false;
                printf("Apostrophe found\n");
                break;

            case '!'...'@':
                found_space = false;
                break;

            case 'A'...'Z': //Will use same range as a...z below it.
            case 'a'...'z':
                found_space = false;
                if(cur_char_id == 0 && !isupper(word[cur_char_id])) //First character in word is not uppercase
                {
                    total_errors++;
                    printf("The first letter of the sentence is not capital.\n");
                }
                else if(cur_char_id > 0 && isupper(word[cur_char_id])) //Any other character in the word is uppercase
                {
                    total_errors++;
                    printf("Capital character detected outside of the first entry.\n");
                }

                word_size++;
                break;
        }
        cur_char_id++;
    }


    if(l > 10) //Only used for words longer than 10 characters
    {
        for (int i = 0; word[i] != '\n'; i++) //Will loop through each character to see which is a hyphen
        {
            if(word[i] != '-')
            {

            }
        }
    }

    printf("%d error(s) found.\n", total_errors);
}


void question9(void)
{
    FILE *filepointer; //q9out is the name of the text file to which the numbers will be outputted
    int real_numbers[20];

    printf("Insert 20 real numbers. Separate each input with a space.\n");

    for(int i = 0; i < 20; i++)
    {
        scanf("%d", &real_numbers[i]);
    }

    filepointer = fopen("C:\\Users\\Jake Sant\\Desktop\\Programming\\C\\Tutorials\\lab3q9.txt","w");
}

/*displayMenu()
{
    printf("Welcome to the shopping system\n");
    printf("Please enter a number to select an option or press q to exit the system\n");
    printf("1. Add items to shopping cart\n2. Show current total\n");
    printf("3. Check out\n4. Cancel session\nq. Quit");

    scanf()
}



void question10(void)
{
    float price[100];
    char item_name[30][100];
    char item_code[8][100]; //8 digit item code

    //Use enums for QUIt, ADD_ITEM, etc... instead of a switch case
    typedef enum InputType
    {
        ADD_ITEM
        QUIT
        ETC...
    } InputType;



    while(true) //always runs
    {
        displayMenu();

        InputType choice = GetInputType();
    }
}*/

int main(void)
{
    question3();

    return 0;
}