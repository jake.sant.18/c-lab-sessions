//
// Created by Jake Sant on 27/10/2019.
//

#include <stdio.h>
#include <string.h>

int main(void)
{
    char w1[21], w2[21], w3[21];
    char r1[21], r2[21], r3[21];

    printf("Kindly enter the first word\n");
    scanf("%s", &w1[0]);
    printf("Kindly enter the second word\n");
    scanf("%s", &w2[0]);
    printf("Kindly enter the third and final word\n");
    scanf("%s", &w3[0]);

    strcpy(r1, w1);
    strcpy(r2, w2);
    strcpy(r3, w3);

    strrev(r1);
    strrev(r2);
    strrev(r3);

    printf("%s reversed is %s\n%s reversed is %s\n%s reversed is %s", w1, r1, w2, r2, w3, r3);

    return 0;
}