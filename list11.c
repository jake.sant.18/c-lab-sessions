//
// Created by Jake Sant on 16/10/2019.
//


#include <stdio.h>

int main(void)
{

    int dogs, cats;

    printf("How many dogs do you have?\n");

    scanf("%d", &dogs);

    printf("How many cats do you have?\n");

    scanf("%d", &cats);

    printf("So you have %d dog(s), %d cat(s) and %d total pet(s)!\n", dogs, cats, dogs+cats);



    return 0;

}

