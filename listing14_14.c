//
// Created by Jake Sant on 04/12/2019.
//

/* booksave.c -- saves structure contents in a file */

//Transfer array from stack to heap

#include <stdio.h>
#include <stdlib.h>
#include "sgets.h" //Copy the header file for this from the notes
#define MAXTITL   40
#define MAXAUTL   40
//#define MAXBKS   10             /* maximum number of books */
#define INCR  5 //Additional elements

typedef struct book /* set up book template  */
{
    char title[MAXTITL];
    char author[MAXAUTL];
    float value;
} book_t;

int main(void)
{
    int maxbks = 10;
    book_t *library; /* pointer to the array of structures on the heap   */
    book_t *temp_library;

    int count = 0;
    int index, filecount;

    FILE * pbooks;

    int size = sizeof(book_t);

    //Allocate memory for library array

    library = (book_t *) malloc(maxbks*size);

    if(library == NULL)
    {
        puts("You're out of memory. \n");
        exit(1);
    }

    if ((pbooks = fopen("book.dat", "a+b")) == NULL)
    {
        fputs("Can't open book.dat file\n",stderr);
        exit(1);
    }

    rewind(pbooks);            /* go to start of file     */

    while (count < maxbks &&  fread(&library[count], size,1, pbooks) == 1)
    {
        if (count == 0)
        {
            puts("Current contents of book.dat:");
        }

        printf("%s by %s: $%.2f\n",library[count].title, library[count].author, library[count].value);

        count++;

        if(count == maxbks)
        {
            //Trigger resize of array

            //realloc(address_of_current_memory_area, new size
            temp_library = realloc(library, (maxbks+INCR)*size);
            if(temp_library != NULL)
            {
                maxbks = INCR;
                printf("Your library now contains a max of %d books\n", maxbks);
            }
        }
    }

    filecount = count;

    if (count == maxbks)
    {
        fputs("The book.dat file is full.", stderr);
        exit(2);
    }

    puts("Please add new book titles.");
    puts("Press [enter] at the start of a line to stop.");
    while (count < maxbks && sgets(library[count].title, MAXTITL) != NULL && library[count].title[0] != '\0')
    {
        puts("Now enter the author.");
        sgets(library[count].author, MAXAUTL);
        puts("Now enter the value.");
        scanf("%f", &library[count++].value);

        while (getchar() != '\n')
        {
            continue;                /* clear input line  */
        }

        if (count < maxbks)
        {
            puts("Enter the next title.");
        }
    }

    if (count > 0)
    {
        puts("Here is the list of your books:");

        for (index = 0; index < count; index++)
        {
            printf("%s by %s: $%.2f\n", library[index].title, library[index].author, library[index].value);
        }
        fwrite(&library[filecount], size, count - filecount, pbooks);
    } else
    {
        puts("No books? Too bad.\n");
    }
    puts("Bye.\n");

    fclose(pbooks);

    free(library); //Frees up space

    return 0;
}