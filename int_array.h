//
// Created by Jake Sant on 18/12/2019.
//

#ifndef TUTORIALS_INT_ARRAY_H
#define TUTORIALS_INT_ARRAY_H

typedef struct _array_int
{
    int * array;
    int length;
} array_int_t;

array_int_t * initIntArray(int size);
void populateArray(array_int_t *temp_array);
void deleteArray(array_int_t *temp_array);
void printValues(array_int_t *temp_array);

#endif //TUTORIALS_INT_ARRAY_H
